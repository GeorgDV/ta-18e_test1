let company = require('./company.js');

describe('company', () => {
    test('company id -> 3', () => {
        expect(company(3).companyId).toBe(3);
        });
    test('company id -> 85', () => {
        expect(company(85).companyId).toBe(85);
        });
    test('company id -> string', () => {
        expect(() => {
            company('fifteen');
        }).toThrow('id needs to be integer');
    });
});