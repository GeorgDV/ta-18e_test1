const toNumber = require('./toNumber');

describe('toNumber', () => {
    test('1 => 1', () => {
    expect(toNumber(1)).toBe(1);
    });

    test('"1" => 1', () => {
        expect(toNumber('1')).toBe(1);
    });

    test('"a" is error', () => {
        expect(() => {
            toNumber('a');
        }).toThrow('value \'a\' is not a number!');
    });

    //ADDED TEST CASES
    test('5 => 5', () => {
        expect(toNumber(5)).toBe(5);
    });

    test('"nine" => error', () => {
        expect(() => {
            toNumber('nine');
        }).toThrow('value \'nine\' is not a number!');
    });
});
