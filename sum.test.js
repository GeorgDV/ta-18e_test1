const sum = require('./sum');

describe('sum', () => {
  it('2 + 3 => 5', () => {
    expect(sum(2, 3)).toBe(5);
  });
  it('4 + 0 => 4', () => {
    expect(sum(4 + 0, 0)).toBe(4);
  });
});
